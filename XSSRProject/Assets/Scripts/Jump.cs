﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Jump : MonoBehaviour
{
    public AudioSource jumpaudiosource;
    public AudioClip[] jumpaudioclip;
    vThirdPersonInput tpInput;
    vThirdPersonController tpController;
    public float VolumeMin = 1f;
    public float pitchMin = 1f;
    public float pitchMax = 1f;
    int lastIndex;
    int newIndex;

    void Start()
    {
        jumpaudiosource = GetComponent<AudioSource>();
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }
    void jump()
    {
        int ClipIndex;
        Randomization();
        jumpaudiosource.volume = Random.Range(0.8f, 1f);
        jumpaudiosource.pitch = Random.Range(0.8f, 1.2f);
        jumpaudiosource.PlayOneShot(jumpaudioclip[newIndex]);
        lastIndex = newIndex;
    }

    void Randomization()
    {
        newIndex = Random.Range(0, jumpaudioclip.Length);
        while (newIndex == lastIndex)
            newIndex = Random.Range(0, jumpaudioclip.Length);

    }

}


