﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBirdsSound : MonoBehaviour
{

    public AudioSource birdsaudiosource;
    public AudioClip birdsaudioclip;

    // Start is called before the first frame update
    void Start()
    {
        birdsaudiosource = gameObject.AddComponent<AudioSource>();
        birdsaudiosource.clip = birdsaudioclip;
        birdsaudiosource.loop = true;
        birdsaudiosource.volume = 0.8f;
        birdsaudiosource.Play();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
