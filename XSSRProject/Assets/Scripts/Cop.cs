﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Cop : MonoBehaviour
{
    private AudioSource source;
    public GameObject SM_Bean_Cop_01;
    public bool collided = false;

    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)

    {
        if (!collided)
        {
            if (other.tag == "Player")
            {
                source.Play();
                collided = true;
            }

        }
    }
    private void OnTriggerExit(Collider other)

    {
        if (other.tag == "Player")
        {
            source.Stop();
            collided = false;
        }
    }
}

 