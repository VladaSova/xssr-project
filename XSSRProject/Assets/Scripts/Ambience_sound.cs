﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ambience_sound : MonoBehaviour
{
    AudioSource audioSource1;
    AudioSource audioSource2;
    public AudioClip outdoorClip;
    public AudioClip indoorClip;
   [Range(0f, 1f)] public float AudioClip1Volume = 1f;
   [Range(0f, 1f)] public float AudioClip2Volume = 1f;
    public float fade = 3f;



    void Start()
    {
        audioSource1 = gameObject.AddComponent<AudioSource>();
        audioSource1.clip = outdoorClip;
        audioSource1.loop = true;
        audioSource1.volume = 0f;
        audioSource1.playOnAwake = false;
        audioSource1.Play();

        audioSource2 = gameObject.AddComponent<AudioSource>();
        audioSource2.clip = indoorClip;
        audioSource2.loop = true;
        audioSource2.volume = 0f;
        audioSource2.playOnAwake = false;
        audioSource2.Play();

        StartCoroutine(Fadein(audioSource1, AudioClip1Volume));
    }



    IEnumerator Fadein(AudioSource source, float volume)
    {
        while (source.volume < volume)
        {
            source.volume += (Time.deltaTime / fade);
            yield return null;
        }
    }
    IEnumerator Fadeout(AudioSource source)
    {
        while (source.volume > 0f)
        {
            source.volume -= (Time.deltaTime / fade);
            yield return null;
        }


    }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
            {

                StartCoroutine(Fadeout(audioSource1));  // Делаем фейдаут для звука улицы
                StartCoroutine(Fadein(audioSource2, AudioClip2Volume)); // Делаем фейдин для звука дома

            }
        }

        private void OnTriggerExit(Collider other) // Функция запускается, если что-то вышло из коллайдера
        {
            if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
            {

                StartCoroutine(Fadein(audioSource1, AudioClip1Volume)); // Делаем фейдин для звука улицы
                StartCoroutine(Fadeout(audioSource2)); // Делаем фейдаут для звука дома

            }
        }

    
}
