﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class JumpEffort : MonoBehaviour
{
    public AudioSource jumpeffortaudiosource;
    public AudioClip[] jumpeffortaudioclip;
    vThirdPersonInput tpInput;
    vThirdPersonController tpController;
    public float VolumeMin = 1f;
    public float pitchMin = 1f;
    public float pitchMax = 1f;
    int lastIndex;
    int newIndex;

    void Start()
    {
        jumpeffortaudiosource = GetComponent<AudioSource>();
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }
    void jumpeffort()
    {
        int ClipIndex;
        Randomization();
        jumpeffortaudiosource.volume = Random.Range(0.8f, 1f);
        jumpeffortaudiosource.pitch = Random.Range(0.9f, 1.1f);
        jumpeffortaudiosource.PlayOneShot(jumpeffortaudioclip[newIndex]);
        lastIndex = newIndex;
    }

    void Randomization()
    {
        newIndex = Random.Range(0, jumpeffortaudioclip.Length);
        while (newIndex == lastIndex)
            newIndex = Random.Range(0, jumpeffortaudioclip.Length);

    }

}
