﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin_Sound : MonoBehaviour
{
    private AudioSource source;
    public GameObject Coin;
    public bool collided = false;
    public GameObject particles;



    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!collided)
        {
            if (other.tag == "Player")
            {
                source.Play(); // Запускаем звук на Аудиосорсе
                particles.GetComponent<ParticleSystem>().Play(); // Ищем компонент в гейм-объекте particles под названием PArticleSystem и включаем эффект
                Destroy(Coin); // Удаляем гейм-объект coin - исчезает монетка
                collided = true; // Пресваеваем переменной collided значетие true = мы пересекли коллайдер
            }
        }
    }
}